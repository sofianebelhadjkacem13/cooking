const crypto = require('crypto');

const hashPassword = (plainText) => {
    return crypto.createHmac('sha256', 'secret key').update(plainText).digest('hex');
}


//  HTTP Code

const unAuthorizied = (res) => {
    res.status(401).send({
        message: 'Unauthorized !'
    });
}

const success = (res) => {
    res.status(200).send({
        message: 'Success !'
    });
}

module.exports = {
    hashPassword,
    unAuthorizied,
    success
};