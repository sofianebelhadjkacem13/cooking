var express = require('express');
var router = express.Router();
const ObjectID = require('mongodb').ObjectID;
const authUtils = require('../utils/auth');

// Configure user account profile edit
// --------------------------------------------------
router.get('/', function (req, res, next) {
  if (!req.isAuthenticated()) {
    authUtils.unAuthorizied(res);
  }
  const users = req.app.locals.users;
  const _id = ObjectID(req.session.passport.user);

  users.findOne({
    _id
  }, (err, results) => {
    if (err) {
      throw err;
    }

    res.render('account', {
      ...results
    });
  });
});
// --------------------------------------------------


// Get public profile for any user
// --------------------------------------------------
router.get('/:username', (req, res, next) => {
  const users = req.app.locals.users;
  const username = req.params.username;

  users.findOne({
    username
  }, (err, results) => {
    if (err || !results) {
      res.render('public-profile', {
        messages: {
          error: ['User not found']
        }
      });
    }

    res.render('public-profile', {
      ...results,
      username
    });
  });
})
// --------------------------------------------------


// Handle updating user profile data
// --------------------------------------------------
router.post('/', (req, res, next) => {
  if (!req.isAuthenticated()) {
    res.redirect('/auth/login');
  }

  const users = req.app.locals.users;
  const {
    name,
    github,
    twitter,
    facebook
  } = req.body;
  const _id = ObjectID(req.session.passport.user);

  users.updateOne({
    _id
  }, {
    $set: {
      name,
      github,
      twitter,
      facebook
    }
  }, (err) => {
    if (err) {
      throw err;
    }

    res.redirect('/users');
  });
});

// Recipes

// TODO: remove this route
router.get('/recipes', (req, res, next) => {
  const message = req.flash();
  res.render('login', {
    message
  });
});

router.post('/recipes', (req, res, next) => {
  if (!req.isAuthenticated()) {
    res.redirect('/auth/login');
  }

  const users = req.app.locals.users;
  const {
    recipes
  } = req.body;
  const _id = ObjectID(req.session.passport.user);
  const user = users.findOne(_id);
  console.log('user', user);
  users.updateOne({
    _id
  }, {
    $set: {
      recipes
    }
  }, (err) => {
    if (err) {
      throw err;
    }

    res.redirect('/users');
  });
});
// --------------------------------------------------

module.exports = router;