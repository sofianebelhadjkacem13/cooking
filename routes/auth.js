const express = require('express');
const router = express.Router();
const authUtils = require('../utils/auth');
const passport = require('passport');

// TODO: remove this route
router.get('/login', (req, res, next) => {
    const message = req.flash();
    res.render('login', {
        message
    });
});

router.post('/login', passport.authenticate('local', {
    failureFlash: 'Wrong username or password',
    failWithError: 'Wrong data'
}), (req, res, next) => {
    authUtils.success(res);
});

router.get('/register', (req, res, next) => {
    const message = req.flash();
    res.render('register', {
        message
    });

});

router.post('/register', (req, res, next) => {
    const registrationParams = req.body;
    const users = req.app.locals.users;
    const payload = {
        username: registrationParams.username,
        password: authUtils.hashPassword(registrationParams.password),
        email: registrationParams.email,
        familyame: registrationParams.familyname
    };

    users.insertOne(payload, (err) => {
        if (err) {
            req.flash('error', 'User account already exists.');
        } else {
            req.flash('success', 'User account registered successfully.');
        }

        res.redirect('/auth/register');
    })
});

router.get('/logout', (req, res, next) => {
    req.session.destroy();
    res.redirect('/');
})

module.exports = router;